#!/usr/bin/env python

'''
COSAR Simulator launcher

Usage ::
	./cosar (GUI version)
	./cosar -nogui /path/to/parfile (Console version)
	
'''

import os
import sys
import argparse
import cosar

# Check Python version
if tuple(sys.version_info[:2]) < (2, 7):
	raise Exception('Python %d.%d is not supported. Please use version 2.6 '
                    'or greater.\n' % tuple(sys.version_info[:2]))

def main(argv):
	"""Launches COSAR Simulator"""
	#Handle command line argument
	parser = argparse.ArgumentParser(description="Simple CoSAR simulation")
	parser.add_argument("-p", "--point_target", action="store_true")
	parser.add_argument("-w", "--hamming", action="store_true")
	parser.add_argument("-a", "--azimuth_points", type=int, default = 256)
	parser.add_argument("-r", "--range_samples", type=int, default = 1024)
	parser.add_argument("-c", "--correlation_coefficient", type=float,default = 0.0)
	args = parser.parse_args()
	naz = args.azimuth_points
	nrg = args.range_samples
	alpha = args.correlation_coefficient
	cosar.simplesim(naz, nrg, alpha=alpha, pt=args.point_target, hamming=args.hamming)
	
	

if __name__ == '__main__':
	main(sys.argv)