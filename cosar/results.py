    # -*- coding: utf-8 -*-

import sys
import os
import time 
import numpy as np
import cosar as cosar
#from scipy import weave
from scipy import signal
import matplotlib.pyplot as plt


def igarss2013(figs,respath=0):
    """Generate figures for IGARSS"""
    if type(respath) != str:
        respath = "/Users/plopezdekker/Documents/WORK/CoSAR/RESULTS/Figures"
    os.chdir(respath)
    if figs == 1:
        filename = "sar_coh_pt_64rg_T40.png"
        cosar.simplesim(256,64,tau=-1,Tint=40.,dt=0,show_SAR=True,show_COSAR=False,dop_rms=0,pt=True,filesave=filename)
        filename = "sar_tau20_pt_64rg_T40.png"
        cosar.simplesim(256,64,tau=20,Tint=40.,dt=0,show_SAR=True,show_COSAR=False,dop_rms=0,pt=True,filesave=filename)
        filename = "sar_tau10_pt_64rg_T40.png"
        cosar.simplesim(256,64,tau=10,Tint=40.,dt=0,show_SAR=True,show_COSAR=False,dop_rms=0,pt=True,filesave=filename)
        filename = "sar_tau5_pt_64rg_T40.png"
        cosar.simplesim(256,64,tau=5,Tint=40.,dt=0,show_SAR=True,show_COSAR=False,dop_rms=0,pt=True,filesave=filename)
        filename = "sar_tau1_pt_64rg_T40.png"
        cosar.simplesim(256,64,tau=1,Tint=40.,dt=0,show_SAR=True,show_COSAR=False,dop_rms=0,pt=True,filesave=filename)
        filename = "sar_tau01_pt_64rg_T40.png"
        cosar.simplesim(256,64,tau=0.1,Tint=40.,dt=0,show_SAR=True,show_COSAR=False,dop_rms=0,pt=True,filesave=filename)
    if figs == 2:
        na = 256
        nr = 256
        Tint = 40.0
        dt = 0
        taus = [20,10,5,1,0.1]
        filenames = ["sar_vs_cosar_tau20_pt_256rg_T40.png",
                     "sar_vs_cosar_tau10_pt_256rg_T40.png",
                     "sar_vs_cosar_tau5_pt_256rg_T40.png",
                     "sar_vs_cosar_tau1_pt_256rg_T40.png",
                     "sar_vs_cosar_tau01_pt_256rg_T40.png"]
        for ind in range(0,len(taus)):
            cosar.simplesim(na,nr,tau=taus[ind],Tint=Tint,dt=dt,show_SAR=True,show_COSAR=True,dop_rms=0,pt=True,filesave=filenames[ind])
    if figs == 3:
        filename = "sar_vs_cosar_tau01_seed1_1024rg_T20.png"
        cosar.simplesim(256,1024,tau=0.01,Tint=20.,dt=0,show_SAR=True,show_COSAR=True,dop_rms=0.5,seed=1,filesave=filename)
        nrs = [128,256,512,1024]
        filenames = ["cosar_tau01_seed1_128rg_T20.png",
                     "cosar_tau01_seed1_256rg_T20.png",
                     "cosar_tau01_seed1_512rg_T20.png",
                     "cosar_tau01_seed1_1024rg_T20.png"]
        for ind in range(0,len(nrs)):
            cosar.simplesim(256,nrs[ind],tau=0.01,Tint=20.,dt=0,show_SAR=False,show_COSAR=True,dop_rms=0.5,seed=1,filesave=filenames[ind])
        