__author__ = "Paco Lopez Dekker"
__email__ = "F.LopezDekker@tudeft.nl"

import numpy as np
import scipy.constants as cnst

def spectral_shift(f0, theta_i, dtheta_i):
    return f0 * dtheta_i / np.tan(theta_i)


def CoSAR_airborne(target_res, h=2.5e3, Bhor=50, v_sar = 100, dv_sar=2, theta_i_deg=45, az_bmwdth_deg=30,
                   f0=np.array([1.24e9, 5.4e9, 9.65e9]),
                   Bw=np.array([200, 200, 500]) * 1e6,
                   antenna_length=None):


    wl0 = cnst.c / f0
    theta_i = np.radians(theta_i_deg)
    U = 5
    tau_c = 3 * wl0 / U
    Bn = np.cos(theta_i) * Bhor
    R0 = h / np.cos(theta_i)
    az_bmwdth = np.radians(az_bmwdth_deg)
    if not antenna_length is None:
        az_bmwdth = wl0 / antenna_length

    # Spectral shift
    dtheta_i = Bn / R0
    df = spectral_shift(f0, theta_i, dtheta_i)
    print("Spectral shift [MHz]")
    print(df/1e6)
    #az_bmwdth = np.radians(25)


    width_gr = R0 *  np.sin(az_bmwdth)

    Lap = wl0 * R0 / 2 / target_res
    T_CoSAR_i = Lap / dv_sar

    ## CoSAR Quality factor
    Bw_common =  Bw - df
    dgr = cnst.c / 2 / Bw_common / np.sin(theta_i)
    Nr = target_res / dgr

    Q = Nr * T_CoSAR_i / tau_c * target_res ** 2 / az_bmwdth

    # Printing of the resuls
    print("Slant range: %f" % R0)

    np.set_printoptions(precision=3)
    print("Azimuth antenna foot print:")
    print(width_gr)
    print("SAR focused res (decorrelation limited)")
    print(wl0 / (v_sar * tau_c)* R0/2)
    print("CoSAR aperture lengths")
    print(Lap)
    print("Ground Distance travelled")
    print(T_CoSAR_i * v_sar)
    print("CoSAR integation time")
    print(T_CoSAR_i)
    print("Number of range gates")
    print(Nr.astype(np.int))
    print("Quality factor (Q):")
    print(Q)
    print("Pseudo SCR (dB):")
    print(5*np.log10(Q))
    np.set_printoptions()