# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 16:13:58 2013

@author: lope_fr
"""


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy import constants
from drama import utils
# csl stands fro cosarlib
import cosar.simplesim as csl
import matplotlib
matplotlib.rcParams['ps.fonttype'] = 42

def geosim1d(cosar, surface, dx, n_rg=1, PRF=0, Tint=300.0, hamming=True,
             n_lags=1, surfaceseed=None, compute_IRF=False, pos_pt=[]):
    """
    Simple 1D simulation using the geometry defined in cosar
    @type cosar csl.CoSARGeometry
    """
    #Init cosar if nothing given. The secquence also forces the
    #assert isinstance(cosar, csl.CoSARGeometry)
    PRF_min = 2*cosar.dDoppler_bandwidth()
    if PRF == 0:
        PRF = PRF_min
    else:
        if PRF < PRF_min:
            print("PRF given to low")
            PRF = PRF_min
    #Check that the azimuth resolution is finer than the minimum lenth scale
    if dx > surface.L_min()/2:
        dx = surface.L_min()/2
    n_az = int(np.round(cosar.L_illuminated()/dx))
    n_t = int(Tint * PRF)
    msg = "Number of azimuth grid samples: " + repr(n_az)
    print(msg)
    msg = "Number of pulses: " + repr(n_t)
    print(msg)
    k0 = 2*np.pi*cosar.f0/constants.c
    #Seed for repeatibility of the surface
    np.random.seed(surfaceseed)
    nrcs, dop, h, az = surface.scene(cosar.L_illuminated(), dx, cosar.f0)
    if compute_IRF:
        nrcs[:] = 0
        if np.size(pos_pt) == 0:
            pos_pt = [0]
        #nrcs[n_az/2 + np.round(1e3/dx)] = 1
        dop[:] = 0
        h[:] = 0
    for pos_this_pt in pos_pt:
        pos_this_pt_s = np.round(pos_this_pt/dx) + n_az/2
        nrcs[pos_this_pt_s] = 1
    #Reseed to randomize realization
    np.random.seed()
    #Time vector
    t_v = np.linspace(-Tint / 2, Tint / 2, int(Tint * PRF))
    #Doppler phase shift for each point on the ground
    dop_phasor = np.exp(2j * np.pi * dop / PRF)
    #The expected amplitude takes into account the grid size and the fact
    #that we are working with circular Gaussian signals
    expected_amp = np.sqrt(nrcs * dx / 2)
    #Now we calculate the trajectory of the two spacecraft assuming linear
    #motions
    pos1 = np.zeros([n_t, 3])
    pos2 = np.zeros([n_t, 3])
    pos1[:, 1] = -np.sin(cosar.theta_i) * cosar.R0()
    pos2[:, 1] = -np.sin(cosar.theta_i - cosar.dtheta_i()) * cosar.R0()
    pos1[:, 2] = np.cos(cosar.theta_i) * cosar.R0()
    pos2[:, 2] = np.cos(cosar.theta_i - cosar.dtheta_i()) * cosar.R0()
    for dim_ind in range(3):
        pos1[:, dim_ind] = pos1[:, dim_ind] + cosar.dv[dim_ind] / 2.0 * t_v
        pos2[:, dim_ind] = pos2[:, dim_ind] - cosar.dv[dim_ind] / 2.0 * t_v
    #Correlation coefficcient
    # alpha**(tau*PRF) = 1/e
    # tau*PRF * log(alpha)=-1 alpha = exp(-1/(tau*PRF)
    alpha = 1.0
    if surface.tau > 0:
        alpha = np.exp(-1.0/(surface.tau*PRF))
        msg = "Pulse to pulse coherence: " + repr(alpha)
        print(msg)
        alpha_end = np.exp(-1.0*t_v.size/(surface.tau*PRF))
        msg = "Start to end coherence: " + repr(alpha_end)
        print(msg)
    if n_rg > 1:
        uscat = np.random.randn(n_az, n_rg) + 1j*np.random.randn(n_az, n_rg)
    else:
        uscat = np.sqrt(2) * np.exp(1j*2*np.pi*np.random.rand(n_az, n_rg))
            #Now main loop
    for t_ind in range(0, t_v.size):
        #Generate random complex scatterers
        uscat = (alpha * uscat * dop_phasor.reshape(n_az, 1) +
                 np.sqrt(1-alpha**2) * (np.random.randn(n_az, n_rg) +
                                        1j * np.random.randn(n_az, n_rg)))
        #Multipliy them with a fixed amplitude
        scat = uscat * expected_amp.reshape(n_az, 1)
        #Calculate ranges from targets to both radars
        r1 = ((np.sqrt((pos1[t_ind, 1])**2 +
              (az - pos1[t_ind, 0])**2 +
              (pos1[t_ind, 2] - h)**2)).reshape(n_az, 1))
        r2 = ((np.sqrt((pos2[t_ind, 1])**2 +
              (az - pos2[t_ind, 0])**2 +
              (pos2[t_ind, 2] - h)**2)).reshape(n_az, 1))
        #Include 2-way phase in field
        s1 = scat * np.exp(-2j*k0*r1)
        s2 = scat * np.exp(-2j*k0*r2)
        # Compute range profiles by integrating in azimuth.
        # This assumed that there is no range migration
        rx1_ = s1.sum(axis=0)
        rx2_ = s2.sum(axis=0)
        # Save in a temporal stack
        if t_ind == 0:
            rx1 = rx1_.copy()
            rx2 = rx2_.copy()
        else:
            rx1 = np.vstack((rx1, rx1_))
            rx2 = np.vstack((rx2, rx2_))
    #Now we have a range-slow time stack of CoSAR data
    n_az_out = n_az
    proc_img = np.zeros([2 * n_lags + 1, n_az_out], dtype=np.complex)
    for lag_ind in range(0, 2*n_lags + 1):
        lag = lag_ind - n_lags
        proc_img[lag_ind, :], T_i_scaling = \
            csl.cosar_proc_1d(rx1, rx2, pos1, pos2, az, k0,
                              han=hamming, dt=lag)
    #Backprojection SAR focusing of one channel
    sar_img, kk = csl.sar_proc_1d(rx1, pos1, az, k0, han=hamming)
    #Amplitude scaling of processed image
    C_p = 2 * cosar.dv[0] / (cosar.wavelength() * cosar.R0())
    amp_scaling = C_p / PRF
    proc_img = amp_scaling * proc_img
    sar_img = amp_scaling * sar_img
    T_i_scaled = T_i_scaling * Tint
    return (az, nrcs, dop, h, proc_img, n_lags, T_i_scaled, dx, n_rg, sar_img)


def geosim1d_montecarlo(cosar, surface, dx, n_rg=1, PRF=0, Tint=300.0,
                        hamming=True, n_lags=1, n_it=10, surfaceseed=1):
    """
    A function to make several runs
    """
    sim_res = geosim1d(cosar, surface, dx, n_rg=n_rg, PRF=PRF, Tint=Tint,
                       n_lags=n_lags, hamming=hamming, surfaceseed=surfaceseed)
    #Unpack first simulation
    az, nrcs, dop, h, proc_img, n_lags, T_i, dx, n_rg, sar_img = sim_res
    dim_res = proc_img.shape
    dim_mtc = (n_it,) + dim_res
    proc_imgs = np.zeros(dim_mtc, dtype=np.complex)
    #Insert output of first simulation in stack
    proc_imgs[0, :, :] = proc_img
    for it in range(1, n_it):
        sim_res = geosim1d(cosar, surface, dx, n_rg=n_rg, PRF=PRF, Tint=Tint,
                           n_lags=n_lags, hamming=hamming,
                           surfaceseed=surfaceseed)
        az, nrcs, dop, h, proc_img, n_lags, T_i, dx, n_rg, sar_img = sim_res
        proc_imgs[it, :, :] = proc_img

    return (az, nrcs, dop, h, proc_imgs, n_lags, T_i, dx, n_rg, PRF)


def geosim1d_save(sim_results, fileroot):
    """
    Function to save simulator results
    """
    #Unpack simulation results
    az, nrcs, dop, h, proc_imgs, n_lags, T_i, dx, n_rg, PRF = sim_results
    filename = fileroot+'_res.npz'
    np.savez(filename, az=az, nrcs=nrcs, dop=dop, h=h, proc_imgs=proc_imgs,
             n_lags=n_lags, T_i=T_i, dx=dx, n_rg=n_rg, PRF=PRF)


def geosim1d_load(fileroot):
    """
    Function to save simulator results
    """
    filename = fileroot+'_res.npz'
    res_d = np.load(filename)
    res = (res_d['az'], res_d['nrcs'], res_d['dop'], res_d['h'],
           res_d['proc_imgs'], res_d['n_lags'], res_d['T_i'],
           res_d['dx'], res_d['n_rg'], res_d['PRF'])
    return (res)


def geosim1d_analyze(cosar, surface, sim_results, dB=False, fileroot=False,
                     fontsize=16):
    """
    Function to analyze output of geosim1d
    """
    matplotlib.rcParams.update({'font.size': fontsize})
    #Unpack simulation results
    az, nrcs, dop, h, proc_imgs, n_lags, T_i, dx, n_rg, PRF = sim_results
    #Calculate Quality factor
    az_res = 0.5 * cosar.wavelength() * cosar.R0() / (cosar.dv[0] * T_i)
    #tau_ca
    tau_ca_Doppler = (cosar.wavelength()*np.sqrt(2) /
                      (4 * np.pi * surface.vr_stdev))
    tau_ca = np.sqrt(1.0 / (1 / surface.tau**2 + 1 / tau_ca_Doppler**2))
    tau_ca_quant = np.max([tau_ca, 1 / PRF])
    Q_nrcs = csl.Q_w(nrcs, nrcs, dx, n_rg, T_i, tau_ca_quant, az_res)
    #Expected standard deviation of the estimated nrcs
    nrcs_err_t = nrcs / np.sqrt(Q_nrcs)
    #Lag 1 expected value
    dop_phase = 2 * np.pi * dop / PRF
    dop_phasor = np.exp(2j * np.pi * dop / PRF)
    #Height phasor
    h_phase = cosar.kz() * h
    h_phasor = np.exp(1j * h_phase)
    corr_ati = 1.0
    if surface.tau > 0:
        corr_ati = np.exp(-1.0 / (surface.tau * PRF))
    R_tau_lag1 = corr_ati * dop_phasor * h_phasor * nrcs
    R_tau_lag1m = corr_ati * np.conj(dop_phasor) * h_phasor * nrcs
    R_tau_lag0 = h_phasor * nrcs
    if np.size(proc_imgs.shape) == 3:
        proc_img = proc_imgs[0, :, :]
        nrcs_err = proc_imgs[:, n_lags, :] - R_tau_lag0.reshape((1, nrcs.size))
        nrcs_err_mean = np.mean(nrcs_err, axis=0)
        nrcs_err_std = np.std(nrcs_err, axis=0)
        plt.figure()
        ax = plt.subplot(1, 1, 1)
        p_std, = plt.plot(az / 1e3, nrcs_err_std, 'b',
                          label=r'std($\delta R_{\tau}$)')
        p_mean_r, = plt.plot(az / 1e3, np.real(nrcs_err_mean), 'c',
                             label=r'$\mathrm{Re}\{\overline{\delta R_{\tau}}\}$')
        p_mean_i, = plt.plot(az / 1e3, np.imag(nrcs_err_mean), 'm',
                             label=r'$\mathrm{Im}\{\overline{\delta R_{\tau}}\}$')
        plt.plot(az / 1e3, nrcs_err_t, 'b--')
        plt.xlabel('Azimuth [km]', fontsize=fontsize)
        plt.ylabel('NRCS errors', fontsize=fontsize)
        ax.set_ylim([-0.05,0.1])
        handles, labels = ax.get_legend_handles_labels()
        l1 = plt.legend([handles[0]], [labels[0]], loc=2)
        l2 = plt.legend(handles[1:], labels[1:], loc=3, ncol=2)
        plt.gca().add_artist(l1)
        if type(fileroot) == str:
            filename = fileroot+'_err_stats.eps'
            plt.savefig(filename)
            filename = fileroot+'_err_stats.png'
            plt.savefig(filename)
        plt.figure()
        plt.plot(az/1e3, np.real(R_tau_lag1))
        for ind in range(1, proc_imgs.shape[0]):
            plt.plot(az/1e3, np.real(proc_imgs[ind, n_lags+1, :]))

    else:
        proc_img = proc_imgs

    #Estimate nrcs
    nrcs_cosar = np.abs(proc_img[n_lags, :])
    #FIXME
    #Show results
        #az in km
    az = az / 1e3
    #plt.figure()
    #csl.plot_nrcs(az, nrcs, az, nrcs_cosar, dB=dB)
    #Unwrap CoSAR using known true phase. Since this is not a phase unwrapping
    #exercise, this is not cheating.
    R_tau_lag0_CoSAR = proc_img[n_lags, :]
    R_tau_lag0_CoSAR_a = np.abs(R_tau_lag0_CoSAR)
    R_tau_lag0_CoSAR_p = (np.angle(R_tau_lag0_CoSAR * np.conj(R_tau_lag0)) +
                          h_phase)
    R_tau_lag1_CoSAR = proc_img[n_lags+1, :]
    R_tau_lag1_CoSAR_a = np.abs(R_tau_lag1_CoSAR)
    R_tau_lag1_CoSAR_p = (np.angle(R_tau_lag1_CoSAR * np.conj(R_tau_lag1)) +
                          dop_phase + h_phase)
    R_tau_lag1m_CoSAR = proc_img[n_lags-1, :]
    R_tau_lag1m_CoSAR_a = np.abs(R_tau_lag1m_CoSAR)
    R_tau_lag1m_CoSAR_p = (np.angle(R_tau_lag1m_CoSAR * np.conj(R_tau_lag1m)) -
                           dop_phase + h_phase)

    plt.figure()
    csl.plot_R_tau(az, abs(R_tau_lag0), h_phase,
                   az, R_tau_lag0_CoSAR_a, R_tau_lag0_CoSAR_p, dB=dB,
                   title=r'$R_{\tau}(x,\tau=0)$', fontsize=fontsize)
    if type(fileroot) == str:
            filename = fileroot+'_R_tau0.eps'
            plt.savefig(filename)
            filename = fileroot+'_R_tau0.png'
            plt.savefig(filename)

    plt.figure()
    csl.plot_R_tau(az, abs(R_tau_lag1), dop_phase + h_phase,
                   az, R_tau_lag1_CoSAR_a, R_tau_lag1_CoSAR_p, dB=dB,
                   title=r'$R_{\tau}(x,\tau=1/PRF)$', fontsize=fontsize)
    if type(fileroot) == str:
            filename = fileroot+'_R_tau1.eps'
            plt.savefig(filename)
            filename = fileroot+'_R_tau1.png'
            plt.savefig(filename)

    plt.figure()
    csl.plot_R_tau(az, abs(R_tau_lag1m), -1.0 * dop_phase + h_phase,
                   az, R_tau_lag1m_CoSAR_a, R_tau_lag1m_CoSAR_p, dB=dB,
                   title=r'$R_{\tau}(x,\tau=-1/PRF)$', fontsize=fontsize)
    if type(fileroot) == str:
            filename = fileroot+'_R_tau-1.eps'
            plt.savefig(filename)
            filename = fileroot+'_R_tau-1.png'
            plt.savefig(filename)


def test_geosim1d():
    """
    Test routine for geosim1d
    """
    #Define geometry and radar
    cs = csl.CoSARGeometry(0.8, 25, 3, 10e9, 10)
    #Define ocean characteristics
    sf = csl.CoSARSurface(0.1, 10, 1, 0, 1000)
    #Run simulation
    sim_res = geosim1d(cs, sf, 250.0, n_rg=100, PRF=10.0, Tint=300.0)


