'''
Created on May 31, 2013

@author: lope_fr
'''
import sys
import time
import numpy as np
#from scipy import weave
from scipy import signal
import matplotlib.pyplot as plt
from scipy import constants
from drama import utils
from drama import geo as sar_geo
#import IPython
import matplotlib
matplotlib.rcParams['ps.fonttype'] = 42
#matplotlib.rcParams['text.usetex'] = True

class CoSARGeometry:
    """A class containing the relevant CoSAR orbital and geometricparameters"""
    _orbit_height = 35786e3

    def __init__(self, dkr, theta_i, dv, f0, antenna_length):
        self.dkr = 1.0*dkr
        self.theta_i = theta_i*np.pi/180
        _dv = 1.0*np.array(dv)
        if _dv.size == 1:
            _dv = np.array([dv, 0.0, 0.0])
        self.dv = _dv
        self.f0 = 1.0*f0
        self.L_antenna = 1.0 * antenna_length

    def wavelength(self):
        return constants.c/self.f0

    def dtheta_i(self):
        res = self.dkr * self.wavelength() / (4 * np.pi * np.cos(self.theta_i))
        return res

    def h_amb(self):
        #res = (2 * np.pi * np.sin(self.theta_i) *
        #       np.cos(self.theta_i) / self.dkr)
        res = (2 * np.pi * np.cos(self.theta_i) /
               (np.sin(self.theta_i) * self.dkr))
        return res

    def kz(self):
        return 2 * np.pi / self.h_amb()

    def R0(self):
        return sar_geo.inc_to_sr(self.theta_i, self._orbit_height)

    def L_illuminated(self):
        return self.wavelength()/self.L_antenna * self.R0()

    def dDoppler_bandwidth(self):
        return 2*self.dv[0]/self.L_antenna


class CoSARSurface:
    """A class defining the scene"""
    def __init__(self, tau, NRCS_stdev, vr_stdev, h_stdev,
                 L_NRCS, L_vr=0, L_h=0,
                 NRCS_mean=-10.0, vr_mean=0.0):
        self.tau = tau
        self.NRCS_stdev = NRCS_stdev
        self.NRCS_mean = NRCS_mean
        self.vr_stdev = vr_stdev
        self.vr_mean = vr_stdev
        self.h_stdev = h_stdev
        self.L_NRCS = L_NRCS
        if L_vr == 0:
            self.L_vr = self.L_NRCS
        else:
            self.L_vr = L_vr
        if L_h == 0:
            self.L_h = 5.0 * self.L_NRCS
        else:
            self.L_h = L_h

    def scene(self, L, dx, f0):
        """Generate a realization of the scene"""
        naz = int(np.round(L/dx))
        az = np.linspace(-naz/2*dx, naz/2*dx, naz)
        wavelength = constants.c/f0
        dop_stdev = 2*self.vr_stdev/wavelength
        dop_mean = 2*self.vr_mean/wavelength
        nrcs = get_nrcs(naz, int(np.round(self.L_NRCS/dx)), pt=False,
                        NRCS_mean=self.NRCS_mean, NRCS_stdev=self.NRCS_stdev)
        dop = get_Doppler(naz, int(np.round(self.L_vr/dx)), dop_stdev)
        dop = dop + dop_mean
        #Reuse get_Doppler function to generate height profile
        h = get_Doppler(naz, int(np.round(self.L_h/dx)), self.h_stdev)
        nrcs.reshape(naz, 1)
        dop.reshape(naz, 1)
        h.reshape(naz, 1)
        return (nrcs, dop, h, az)

    def L_min(self):
        """Returns the minimum length scale"""
        return np.min(np.array([self.L_h, self.L_NRCS, self.L_vr]))


def Q_w(R, NRCS, dx, N_r, T_i, tau_ca, az_res, F_n=1.0):
    """
    Estimate CoSAR imaging quality.
    R is the space-dependent expected correlation
    NRCS is the corresponding NRCS
    dx is the grid spacing
    N_r the number of range gates averaged
    T_i the integration time
    tau_ca the decorrelation time
    az_res the azimuth resolution
    """
    Q_scaling = N_r / F_n * T_i / tau_ca * az_res**2
    Power = np.sum(NRCS) * dx
    Q_w = Q_scaling * np.abs(R)**2 / Power**2
    return Q_w


def get_nrcs(naz, gaussian_length, pt=False, SCR=30,
             NRCS_mean=-10, NRCS_stdev=10.0):
    """Generate a random but smooth NRCS"""
    #We use a uniform distribution
    dist_amp = np.sqrt(12) * NRCS_stdev
    nrcs_db = dist_amp * np.random.random(naz) - dist_amp / 2 + NRCS_mean
    #A Gaussian Filter
    gfilt = signal.gaussian(int(naz / 4), gaussian_length, sym=True)
    gfilt = gfilt / gfilt.sum()
    nrcs_db_filt = signal.fftconvolve(nrcs_db, gfilt, mode='same')
    nrcs_filt = utils.db2lin(nrcs_db_filt)
    if pt:
        nrcs_filt[:] = utils.db2lin(-SCR)
        nrcs_filt[[naz / 4, naz / 2]] = 1
    return nrcs_filt


def get_Doppler(naz, gaussian_length, dop_rms=1):
    """Generate random Doppler frequencies"""
    dop = np.random.randn(naz)
    #A Gaussian Filter
    gfilt = signal.gaussian(int(naz / 4), gaussian_length, sym=True)
    gfilt = gfilt / gfilt.sum()
    dop_filt = signal.fftconvolve(dop, gfilt, mode='same')
    norm_factor = dop_rms/np.std(dop_filt)
    dop_filt = dop_filt * norm_factor
    return dop_filt


def cosar_proc_1d(rx1, rx2, pos1, pos2, az, k0, han=False, dt=0):
    """CoSAR processing assuming no range migrations"""
    #Cross correlate signals
    if dt == 0:
        corr_est = np.mean(rx2 * np.conjugate(rx1), axis=1)
    else:
        if dt > 0:
            corr_p2p = rx2[dt:, :] * np.conjugate(rx1)[:-dt, :]
        else:
            corr_p2p = rx2[0:dt, :] * np.conjugate(rx1)[-dt:, :]
        corr_est = np.mean(corr_p2p, axis=1)

    naz = az.size
    nt = corr_est.size
    if han:
        az_win = np.hanning(nt)
    else:
        az_win = np.ones(nt)

    T_i_scaling = np.mean(az_win**2)
    #Result
    s_est = np.zeros(naz, dtype=np.complex)
    for t_ind in range(0, corr_est.size):
        #Calculate ranges from targets to both radars
        #r_2 = ((pos1[t_ind, 1])**2 + (az - pos1[t_ind, 0])**2 +
        #       pos1[t_ind, 2] ** 2 )
        r1 = np.sqrt((pos1[t_ind, 1])**2 + (az - pos1[t_ind, 0])**2 +
                     pos1[t_ind, 2]**2)

        r2 = np.sqrt((pos2[t_ind + dt, 1])**2 +
                     (az - pos2[t_ind + dt, 0])**2 +
                     pos2[t_ind+dt, 2]**2)

        dphase = 2*k0*(r2-r1)
        phasor = np.exp(1j*dphase)
        s_est = s_est + az_win[t_ind] * phasor * corr_est[t_ind]
    #Normalize
    s_est = s_est  # / corr_est.size / 2
    return (s_est, T_i_scaling)


def sar_proc_1d(rx, pos, az, k0, han=False):
    """SAR processing assuming no range migrations using backprojection"""
    nt = rx.shape[0]
    nrg = rx.shape[1]
    naz = az.size
    if han:
        az_win = np.hanning(nt)
    else:
        az_win = np.ones(nt)
    #Results
    T_i_scaling = np.mean(az_win**2)
    s_est = np.zeros([naz, nrg], dtype=np.complex)
    for t_ind in range(0, nt):
        r = (np.sqrt((pos[t_ind, 1])**2 + (az - pos[t_ind, 0])**2 +
             pos[t_ind, 2]**2))
        phase = 2 * k0 * r
        phasor = np.exp(1j * phase)
        s_est = s_est + az_win[t_ind] * rx[t_ind, :] * phasor.reshape([naz, 1])
    pow_est = np.mean(np.abs(s_est)**2, axis=1)
    return (pow_est/nt, T_i_scaling)


def plot_nrcs(az_true, nrcs_true, az_proc, nrcs_proc, dB=True, nrcs_err=0):
    """Routine to plot cosar estimated NRCS and ground truth"""
    ax = plt.subplot(1, 1, 1)
    res_dim = nrcs_proc.shape
    if np.size(res_dim) == 2:
        n_res = res_dim[0]
    else:
        n_res = 1
        nrcs_proc.shape = [n_res, res_dim[0]]
    if dB:
        p_true_nrcs, = plt.plot(az_true, utils.db(nrcs_true, norm=True), 'b',
                                label="True")
        p_cosar_nrcs, = plt.plot(az_proc, utils.db(nrcs_proc[0, :], norm=True),
                                 'r', label="CoSAR")
        for it_res in range(1, n_res):
            plt.plot(az_proc, utils.db(nrcs_proc[it_res, :], norm=True), 'r')
        if np.size(nrcs_err) == np.size(nrcs_true):
            p_error, = plt.plot(az_true,
                                utils.db(nrcs_true + nrcs_err, norm=True),
                                'b--')

        plt.xlabel('Azimuth [km]')
        plt.ylabel('NRCS [dB]')
    else:
        p_true_nrcs, = plt.plot(az_true, nrcs_true, 'b',
                                label="True")
        p_cosar_nrcs, = plt.plot(az_proc, nrcs_proc[0, :], 'r',
                                 label="CoSAR")
        for it_res in range(1, n_res):
            plt.plot(az_proc, nrcs_proc[it_res, :], 'r')
        if np.size(nrcs_err) == np.size(nrcs_true):
            p_error, = plt.plot(az_true,
                                nrcs_true + nrcs_err, 'b--')
        plt.xlabel('Azimuth [km]')
        plt.ylabel('NRCS')
    #Add labels
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(handles, labels, loc=3)
    plt.show()


def plot_R_tau(az_true, r_true_a, r_true_p,
               az_proc, r_proc_a, r_proc_p, dB=True, r_err=0,
               title=False, fontsize=12):
    """
    Routine to plot CoSAR estimated R_tau and ground truth
    Variables ending in _a are amplitude, those ending in _p are phases
    """
    ax = plt.subplot(2, 1, 1)
    res_dim = r_proc_a.shape
    if np.size(res_dim) == 2:
        n_res = res_dim[0]
    else:
        n_res = 1
        r_proc_a.shape = [n_res, res_dim[0]]
        r_proc_p.shape = [n_res, res_dim[0]]
    if dB:
        p_true_r, = plt.plot(az_true, utils.db(r_true_a, norm=True), 'b',
                             label="True")
        p_cosar_r, = plt.plot(az_proc,
                              utils.db(r_proc_a[0, :], norm=True),
                              'r', label="CoSAR")
        for it_res in range(1, n_res):
            plt.plot(az_proc,
                     utils.db(r_proc_a[it_res, :], norm=True),
                     'r')
        if np.size(r_err) == np.size(r_true_a):
            p_error, = plt.plot(az_true,
                                utils.db(r_true_a + r_err, norm=True),
                                'b--')

        #plt.xlabel('Azimuth [km]')
        plt.ylabel(r'$|R|$ $\mathrm{[dB]}$', fontsize=fontsize)
    else:
        p_true_r, = plt.plot(az_true, r_true_a, 'b',
                             label="True")
        p_cosar_r, = plt.plot(az_proc, r_proc_a[0, :], 'r',
                              label="CoSAR")
        for it_res in range(1, n_res):
            plt.plot(az_proc, r_proc_a[it_res, :], 'r')
        if np.size(r_err) == np.size(r_true_a):
            p_error, = plt.plot(az_true,
                                r_true_a + r_err, 'b--')
        #plt.xlabel('Azimuth [km]')
        plt.ylabel(r'$|R|$', fontsize=fontsize)
    if type(title) == str:
        plt.title(title, fontsize=fontsize)
    #Add labels
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(handles, labels, loc=3, fontsize=fontsize)
    #Now plot the phase
    ax = plt.subplot(2, 1, 2)
    p_true_r_ph, = plt.plot(az_true, r_true_p, 'b', label='True')
    p_true_r_ph, = plt.plot(az_proc, r_proc_p[0, :], 'r', label='CoSAR')
    for it_res in range(1, n_res):
        plt.plot(az_proc, r_proc_p[it_res, :], 'r')

    plt.xlabel('Azimuth [km]', fontsize=fontsize)
    plt.ylabel(r'$\mathrm{arg}(R)$ $\mathrm{[rad]}$', fontsize=fontsize)



def simplesim_show(az,true_nrcs,est_nrcs,true_Doppler=0,est_Doppler=0,sar_nrcs=0,
                   show_SAR=False,show_Doppler=False,show_COSAR=True,filesave=0):
    """Routine to plot simplesim simulation results"""
    if show_Doppler:
        ax = plt.subplot(2,1,1)
    else:
        ax = plt.subplot(1,1,1)
    p_true_nrcs, = plt.plot(az,utils.db(true_nrcs,norm=True),'b',label="True")
    if show_COSAR:
        p_cosar_nrcs, = plt.plot(az,utils.db(est_nrcs,norm=True),'r',label="CoSAR")
    plt.xlabel('Azimuth')
    plt.ylabel('Intensity [dB]')
    if show_SAR:
        plt.plot(az,utils.db(sar_nrcs,norm=True),'k--',label="SAR")
    #Add labels
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(handles,labels,loc=3)
    if show_Doppler > 0:
        plt.subplot(2,1,2)
        plt.plot(az,true_Doppler,'b',az,est_Doppler,'r')
        plt.xlabel('Azimuth')
        plt.ylabel('Doppler [Hz]')
    plt.show()
    #Check if we want to print this
    if type(filesave) == str:
        plt.savefig(filesave)
        plt.close()

def simplesim_scene_init(naz,length_scale,dop_rms=1.0,pt='False'):
    nrcs = get_nrcs(naz,length_scale,pt=pt)
    dop = get_Doppler(naz,10,dop_rms)
    nrcs.reshape(naz,1)
    dop.reshape(naz,1)
    return (nrcs,dop)

def simplesim(naz,nrg,tau=0.0,pt=False,hamming=True,dop_rms=1.0,Tint=20.0,dt=0,
              show_SAR=False,show_COSAR=True,filesave=0,seed=None):

    #Create some complexarray of random numbers

    dx = 1.0
    #Spacecraft height and ground range
    z1 = z2 = 5e3
    x1 = x2 = -5e3
    R0 = 10e3
    v1, v2 = 1.0, -1.0
    f0 = 10e9
    k0 = 2*np.pi*f0/constants.c
    l0 = constants.c/f0
    fDop_max = naz * dx / R0 * 2 * abs(v2-v1)/l0
    PRF = 8*fDop_max
    print("PRF = %i", int(PRF))
    t_v = np.linspace(-Tint/2, Tint/2, Tint*PRF)
    az = np.linspace(-naz/2*dx, naz/2*dx, naz)
    ##Seed simulator if needed
    np.random.seed(seed)
    #nrcs
    nrcs, dop = simplesim_scene_init(naz, 10, pt=pt)
    dop_phasor = np.exp(2j*np.pi*dop/PRF)
    #print dop_phasor.shape
    expected_amp = np.sqrt(nrcs)
    #teporal decorrelation factor or whatever
    # Get trajectory of spacecraft
    pos1 = np.zeros([Tint*PRF, 3])
    pos2 = np.zeros([Tint*PRF, 3])
    pos1[:, 0] = v1*t_v
    pos2[:, 0] = v2*t_v
    pos1[:, 1] = x1
    pos2[:, 1] = x2
    pos1[:, 2] = z1
    pos2[:, 2] = z2
    #Correlation coefficcient
    # alpha**(tau*PRF) = 1/e
    # tau*PRF * log(alpha)=-1 alpha = exp(-1/(tau*PRF)
    alpha = 1
    if tau > 0:
        alpha = np.exp(-1.0/(tau*PRF))
    if nrg > 1:
        uscat = np.random.randn(naz, nrg) + 1j*np.random.randn(naz, nrg)
    else:
        uscat = np.exp(1j*2*np.pi*np.random.rand(naz, nrg))
    for t_ind in range(0, t_v.size):
        #Generate random complex scatterers
        uscat = (alpha * uscat * dop_phasor.reshape(naz, 1) +
                 np.sqrt(1-alpha**2) * (np.random.randn(naz, nrg) +
                                        1j * np.random.randn(naz, nrg)))
        #Multipliy them with a fixed amplitude
        scat = uscat * expected_amp.reshape(naz, 1)
        #Calculate ranges from targets to both radars
        r1 = ((np.sqrt((pos1[t_ind, 1])**2 +
              (az - pos1[t_ind, 0])**2 + pos1[t_ind, 2]**2)).reshape(naz, 1))
        r2 = ((np.sqrt((pos2[t_ind, 1])**2 +
              (az - pos2[t_ind, 0])**2 + pos2[t_ind, 2]**2)).reshape(naz, 1))
        #Include 2-way phase in field
        s1 = scat * np.exp(-2j*k0*r1)
        s2 = scat * np.exp(-2j*k0*r2)
        # Compute range profiles by integrating in azimuth.
        # This assumed that there is no range migration
        rx1_ = s1.sum(axis=0)
        rx2_ = s2.sum(axis=0)
        # Save in a temporal stack
        if t_ind == 0:
            rx1 = rx1_.copy()
            rx2 = rx2_.copy()
        else:
            rx1 = np.vstack((rx1, rx1_))
            rx2 = np.vstack((rx2, rx2_))
    #Now we have a range-slow time stack of CoSAR data
    proc_img, T_i_scaling = cosar_proc_1d(rx1, rx2, pos1, pos2, az, k0,
                                          han=hamming)
    if dt > 0:
        proc_ximg = cosar_proc_1d(rx1, rx2, pos1, pos2, az, k0, han=hamming,
                                  dt=dt)
        #The phase of this should be the Doppler phase
        est_Doppler = np.angle(proc_ximg)/(dt/PRF)/(2*np.pi)
    else:
        est_Doppler = 0

    if show_SAR:
        sar_img = sar_proc_1d(rx1, pos1, az, k0, han=hamming)
    else:
        sar_img = 0
    #IPython.embed()
    #Show results
    #plt.ion()
    simplesim_show(az, nrcs, np.abs(proc_img), dop, est_Doppler, sar_img,
                   show_SAR=show_SAR, show_Doppler=(dt > 0),
                   show_COSAR=show_COSAR, filesave=filesave)
    print("Done!")

