"""
===================================
Input and output (:mod:`pycosar`)
===================================

.. currentmodule:: pycosar

   
"""

from simplesim import *
from results import *
from geosim1d import *
#from rat import *

__all__ = ['simplesim','results']