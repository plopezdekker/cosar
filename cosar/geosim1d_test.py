# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 10:29:26 2014

@author: Paco López Dekker
"""
#%%
from cosar.geosim1d import geosim1d_analyze, geosim1d, geosim1d_montecarlo,\
                     geosim1d_save, geosim1d_load
import cosar.simplesim as csl
import os

#%%
n_it = 20
v_stdev = 1.0  # Small to avoid big ambiguity issues
h_stdev = 1.0
dv = 6.0
PRF = 200.0
tau = 0.01
Tint = 30.0

respath = "/Users/plopezdekker/Documents/WORK/CoSAR/RESULTS/Figures"
os.chdir(respath)
#Define geometry and radar
cs = csl.CoSARGeometry(0.8, 25, dv, 10e9, 10)
#Define ocean characteristics
sf = csl.CoSARSurface(tau, 10, v_stdev, h_stdev, 1000)
#Run simulations
Tint = 100
sim_res = geosim1d_montecarlo(cs, sf, 250.0, n_rg=10, PRF=PRF, Tint=Tint,
                              surfaceseed=1, n_it=n_it)
geosim1d_save(sim_res, 'sim_PRF200_Tint100_nr10')
sim_res = geosim1d_load('sim_PRF200_Tint100_nr10')
geosim1d_analyze(cs, sf, sim_res, fileroot='sim_PRF200_Tint100_nr10', dB=True)

Tint = 300
#sim_res = geosim1d_montecarlo(cs, sf, 250.0, n_rg=10, PRF=PRF, Tint=Tint,
#                              surfaceseed=1, n_it=n_it)
#geosim1d_save(sim_res, 'sim_PRF200_Tint300_nr10')
sim_res = geosim1d_load('sim_PRF200_Tint300_nr10')
geosim1d_analyze(cs, sf, sim_res, fileroot='sim_PRF200_Tint300_nr10', dB=True)

Tint = 600
#sim_res = geosim1d_montecarlo(cs, sf, 250.0, n_rg=10, PRF=PRF, Tint=Tint,
#                              surfaceseed=1, n_it=n_it)
#geosim1d_save(sim_res, 'sim_PRF200_Tint600_nr10')
sim_res = geosim1d_load('sim_PRF200_Tint600_nr10')
geosim1d_analyze(cs, sf, sim_res, fileroot='sim_PRF200_Tint600_nr10', dB=True)

# %%
