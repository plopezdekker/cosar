from setuptools import setup, find_packages

setup(
    name='cosar',
    version='24.03.26',
    # packages=['oceansar'],
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    # package_dir={'': 'oceansar'},
    # url='url = https://gitlab.tudelft.nl/plopezdekker/s1sea',
    license='GPL-3.0',
    author='Paco Lopez Dekker',
    author_email='F.LopezDekker@tudelft.nl',
    description='',
    install_requires=['numpy', 'scipy', 'matplotlib', 'drama']
)
