# CoSAR



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.tudelft.nl/plopezdekker/cosar.git
git branch -M main
git push -uf origin main
```



## Name
CoSAR (Correlating Synthetic  Aperture Radar).

## Description
Implements some simple simulations of the CoSAR concept, as described in (https://ieeexplore.ieee.org/abstract/document/7339704)


## Roadmap
No roadmap at the moment since busy with other things.

## Contributing
Let the author  know if you are interested in contributing to the project.

## Authors 
Code written by Paco López-Dekker. 

## License
See license file.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
